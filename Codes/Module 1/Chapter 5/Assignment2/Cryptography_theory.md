<span style="color:blue ;margin-left:10em;"><b>FROM DES TO 3-DES</b>
</span>

> DEFINITION

 DES is a Block cipher, which takes
64-bit plain text and creates a 64-bit
cipher text.

DES is an implementation of a Feistel Cipher. It uses 16 round Feistel structure. The block size is 64-bit. Though, key length is 64-bit, DES has an effective key length of 56 bits, since 8 of the 64 bits of the key are not used by the encryption algorithm (function as check bits only).


For a very brief theory of Digital encryption Standard and their analysis, [click here](http://cse29-iiith.vlabs.ac.in/exp6/media/DES1.pdf)

<span style="width:500px;height:500px">[![DES](dess.jpg)](http://cse29-iiith.vlabs.ac.in/exp6/media/DES1.pdf)
</span>