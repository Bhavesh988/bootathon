var s1:HTMLSelectElement=<HTMLSelectElement>document.getElementById("s1");//select id
var li:{value:number,name:string}[]=[];//list for select drop menu options
    li.push({value:0,name:"Select"});
    li.push({value:1,name:"X^2 versus X"});
    li.push({value:2,name:"X^2+X versus X^2"});
    li.push({value:3,name:"Square"});
    li.push({value:4,name:"Log x"});
    li.push({value:5,name:"Log y"});
    for(let i=0;i<=9;i++)
    {
        let option:HTMLOptionElement=<HTMLOptionElement>document.createElement("option");//adding options 
        option.text=li[i].value.toString()+"."+li[i].name;
        option.value=li[i].value.toString();
        s1.add(option);
    }
function create(){//function to check value of select drop menu
    var sel:HTMLInputElement=<HTMLInputElement>document.getElementById("s1");
    for(let i=0;i<9;i++){  
        if(sel.value==li[i].value.toString())
        {
            gra(li[i].value);
        }
    }
}
declare var drawgraph2;//framework functions
declare var drawgraph;
declare var graphline;
declare var graphlogx;
declare var graphlogy;
declare var logymultiple;
function gra(n1:number){
    if(n1==1)// x^2 vs x
    {
        var datapoints1:{x:number,y:number}[]=[];
        for(let i=1;i<=20;i++){
            datapoints1.push({x:i,y:i*i});
        }
        drawgraph("wew",datapoints1,"X-axis","Y-axis");//drawing graph
    }
    else if(n1==2)//x^2+x vs x and x^2 vs x
    {
        var datapoints1:{x:number,y:number}[]=[];
        var datapoints2:{x:number,y:number}[]=[];
        for(let i=1;i<=20;i++){
            datapoints1.push({x:i,y:i*i});
            datapoints2.push({x:i,y:i*i+i});
        }
        drawgraph2("wew",datapoints1,datapoints2,"X-axis","Y-axis","Comparison","x^2","x^2+x");//drawing graph
    }
    else if(n1==3)//square
    {
        var x1:number=+prompt("Enter starting cooridnate x1");
        var y1:number=+prompt("Enter starting cooridnate y1");
        var dis:number=+prompt("Enter lenght of square side");
        var datapoints:{x:number,y:number}[]=[];
        datapoints.push({x:x1,y:y1});
        datapoints.push({x:x1+dis,y:y1});
        datapoints.push({x:x1+dis,y:y1+dis});
        datapoints.push({x:x1,y:y1+dis});
        datapoints.push({x:x1,y:y1});
        graphline("wew",datapoints,"X-axis","Y-axis");//drawing lines
    }
    else if(n1==5)//logy
    {
        var datapoints3:{x:number,y:number}[]=[];
        datapoints3.push({x:10,y:10});
        datapoints3.push({x:15,y:640});
        datapoints3.push({x:22,y:10240});
        datapoints3.push({x:31,y:80240});
        datapoints3.push({x:43,y:162400});
        datapoints3.push({x:55,y:200000});
        datapoints3.push({x:70,y:220000});
        datapoints3.push({x:87,y:230000});
        datapoints3.push({x:100,y:256000});

        graphlogy("wew",datapoints3,"X-axis","Y-axis");//drawing curve
    }
    else if(n1==4)//logx
    {
        var datapoints3:{x:number,y:number}[]=[];
        datapoints3.push({x:10,y:10});
        datapoints3.push({x:15,y:640});
        datapoints3.push({x:22,y:10240});
        datapoints3.push({x:31,y:80240});
        datapoints3.push({x:43,y:162400});
        datapoints3.push({x:55,y:200000});
        datapoints3.push({x:70,y:220000});
        datapoints3.push({x:87,y:230000});
        datapoints3.push({x:100,y:256000});
        graphlogx("wew",datapoints3,"X-axis","Y-axis");
    }
}