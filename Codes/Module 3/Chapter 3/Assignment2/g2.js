var s1 = document.getElementById("s1"); //select item
var li = []; //list 
li.push({ value: 0, name: "Select" });
li.push({ value: 1, name: "Sine Curve" });
li.push({ value: 2, name: "Cos Curve" });
for (let i = 0; i <= 2; i++) {
    let option = document.createElement("option"); //adding options to drop down menu
    option.text = li[i].value.toString() + "." + li[i].name;
    option.value = li[i].value.toString();
    s1.add(option);
}
function create() {
    var sel = document.getElementById("s1");
    for (let i = 0; i < 3; i++) {
        if (sel.value == li[i].value.toString()) {
            gra(li[i].value);
        }
    }
}
function gra(n1) {
    if (n1 == 1) //sine curve
     {
        var datapoints1 = []; //datapoints
        datapoints1.push({ x: 0, y: 0 });
        datapoints1.push({ x: (Math.PI / 4), y: Math.sin(Math.PI / 4) });
        datapoints1.push({ x: (Math.PI / 2), y: Math.sin(Math.PI / 2) });
        datapoints1.push({ x: (3 * Math.PI / 4), y: Math.sin(3 * Math.PI / 4) });
        datapoints1.push({ x: (Math.PI), y: Math.sin(Math.PI) });
        datapoints1.push({ x: (5 * Math.PI / 4), y: Math.sin(5 * Math.PI / 4) });
        datapoints1.push({ x: (3 * Math.PI / 2), y: Math.sin(3 * Math.PI / 2) });
        datapoints1.push({ x: (7 * Math.PI / 4), y: Math.sin(7 * Math.PI / 4) });
        datapoints1.push({ x: (2 * Math.PI), y: Math.sin(2 * Math.PI) });
        drawgraph("wew", datapoints1, "X-axis", "Y-axis"); //drawing curve
    }
    else if (n1 == 2) //cos curve
     {
        var datapoints1 = []; //datapoints
        datapoints1.push({ x: 0, y: 1 });
        datapoints1.push({ x: (Math.PI / 4), y: Math.cos(Math.PI / 4) });
        datapoints1.push({ x: (Math.PI / 2), y: Math.cos(Math.PI / 2) });
        datapoints1.push({ x: (3 * Math.PI / 4), y: Math.cos(3 * Math.PI / 4) });
        datapoints1.push({ x: (Math.PI), y: Math.cos(Math.PI) });
        datapoints1.push({ x: (5 * Math.PI / 4), y: Math.cos(5 * Math.PI / 4) });
        datapoints1.push({ x: (3 * Math.PI / 2), y: Math.cos(3 * Math.PI / 2) });
        datapoints1.push({ x: (7 * Math.PI / 4), y: Math.cos(7 * Math.PI / 4) });
        datapoints1.push({ x: (2 * Math.PI), y: Math.cos(2 * Math.PI) });
        drawgraph("wew", datapoints1, "X-axis", "Y-axis"); //drawing cos curve
    }
}
//# sourceMappingURL=g2.js.map