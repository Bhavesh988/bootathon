var t1 = document.getElementById("t1");
var table1 = document.getElementById("table1");
var c1 = 1;
var c3, c2, c = 1;
function exceptio(t1) {
    if (isNaN(+t1.value)) { //Exception handling to check if number is undefined or not
        alert("Enter valid number");
        c1 = 1;
    }
    else {
        c1 = 0;
    }
    if (parseInt(t1.value) != +t1.value || +t1.value < 0) //Exception handling to check if the number is an integer or not also if value is +ve or not
     {
        alert("Enter +ve integer numbers only");
        c3 = 1;
    }
    else {
        c3 = 0;
    }
    if (t1.value.length == 0) //Exception handling to check if a number has been inputted or not
     {
        alert("Input a number !");
        c = 1;
    }
    else {
        c = 0;
    }
    if (+t1.value == 1) //Exception handling to check for min no.of forces required
     {
        alert("There shld be atleast 2 forces");
        c2 = 1;
    }
    else {
        c2 = 0;
    }
}
function go() {
    exceptio(t1);
    if (c1 == 0 && c == 0 && c2 == 0 && c3 == 0) {
        deltab(table1);
        cretab(table1, "a");
    }
}
function deltab(table1) {
    while (table1.rows.length > 0) {
        table1.deleteRow(0);
    }
}
function cretab(table1, id) {
    for (let i = 0; i < +t1.value; i++) {
        var row = table1.insertRow(); //row creation
        for (let j = 0; j < 2; j++) {
            var cell = row.insertCell(); //cell creation
            var tt = document.createElement("input");
            tt.type = "text";
            tt.id = id + i + j;
            cell.appendChild(tt);
        }
    }
}
function read_val(arr, row, col, id) {
    for (let i = 0; i < +t1.value; i++) {
        arr[i] = [];
        for (let j = 0; j < 2; j++) {
            let tt = document.getElementById(id + i + j); //reading values from table using unqiue ids
            arr[i][j] = +tt.value;
        }
    }
}
function resultant(arr) {
    var fx = 0;
    var fy = 0;
    var ag;
    for (let i = 0; i < +t1.value; i++) {
        fx = fx + arr[i][0] * Math.cos(Math.PI / 180 * arr[i][1]);
        fy = fy + arr[i][0] * Math.sin(Math.PI / 180 * arr[i][1]);
    }
    var resultant = Math.sqrt(Math.pow(fx, 2) + Math.pow(fy, 2));
    ag = Math.atan2(fy, fx);
    document.getElementById("ans").innerHTML = "The magnitude of forces are " + resultant + " and its angle in radians is " + ag;
}
function start() {
    var arr = [];
    read_val(arr, +t1.value, 2, "a");
    resultant(arr);
}
//# sourceMappingURL=app.js.map