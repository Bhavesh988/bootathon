class Line {
    constructor(rx, ry, len, canvas, context) {
        this.rx = rx;
        this.ry = ry;
        this.len = len;
        this.canvas = canvas;
        this.context = context;
    }
    draw1() {
        this.context.beginPath();
        this.context.moveTo(this.rx, this.ry);
        this.context.lineTo(this.rx + 50, this.ry);
        this.context.moveTo(this.rx + 50, this.ry - 20);
        this.context.lineTo(this.rx + 50, this.ry + 80);
        this.context.moveTo(this.rx + 50, this.ry + 60);
        this.context.lineTo(this.rx, this.ry + 60);
        this.context.moveTo(this.rx + 50, this.ry - 20);
        this.context.lineTo(this.rx + 80, this.ry - 20);
        this.context.moveTo(this.rx + 50, this.ry + 80);
        this.context.lineTo(this.rx + 80, this.ry + 80);
        this.context.lineWidth = 3;
        this.context.stroke();
    }
}
class AND {
    //public B:Line;
    //public C Line;
    // public D:Circle;
    constructor(rx, ry, len, canvas, context) {
        this.rx = rx;
        this.ry = ry;
        this.len = len;
        this.canvas = canvas;
        this.context = context;
    }
    draw() {
        this.A = new Line(this.rx, this.ry, this.len, this.canvas, this.context);
        this.A.draw1();
    }
}
//# sourceMappingURL=app_2.js.map