class Line {
    constructor(rx, ry, angle, len, canvas, context) {
        this.rx = rx;
        this.ry = ry;
        this.len = len;
        this.angle = angle;
        this.canvas = canvas;
        this.context = context;
    }
    draw1() {
        this.context.beginPath();
        this.context.moveTo(this.rx, this.ry);
        this.context.lineTo(this.rx + (this.len * Math.cos(this.angle)), this.ry + (this.len * Math.sin(this.angle)));
        this.context.strokeStyle = "black";
        this.context.lineWidth = 2;
        this.context.stroke();
    }
}
class Circle {
    constructor(rx, ry, r, canvas, context) {
        this.rx = rx;
        this.ry = ry;
        this.r = r;
        this.canvas = canvas;
        this.context = context;
    }
    draw1() {
        this.context.beginPath();
        this.context.arc(this.rx, this.ry, this.r, 0, 2 * Math.PI, true);
        this.context.strokeStyle = "black";
        this.context.fillStyle = "black";
        this.context.fill();
        this.context.lineWidth = 2;
        this.context.stroke();
    }
}
class Curve {
    constructor(rx, ry, r, canvas, context) {
        this.rx = rx;
        this.ry = ry;
        this.r = r;
        this.canvas = canvas;
        this.context = context;
    }
    draw1() {
        context.beginPath();
        context.lineTo(this.rx, this.ry + this.r * Math.sin(Math.PI / 180));
        for (let i = 0; i < 360; i++) {
            context.lineTo(this.rx + i, this.ry + this.r * Math.sin(Math.PI * i / 180));
        }
        context.lineWidth = 2.5;
        context.strokeStyle = "blue";
        context.stroke();
        context.beginPath();
        this.ry = this.ry + 200;
        context.lineTo(this.rx, this.ry + this.r * Math.cos(Math.PI / 180));
        for (let i = 0; i < 360; i++) {
            context.lineTo(this.rx + i, this.ry + this.r * Math.cos(Math.PI * i / 180));
        }
        context.lineWidth = 2.5;
        context.strokeStyle = "red";
        context.stroke();
    }
}
class pendulum {
    constructor(rx, ry, angle, len, canvas, context) {
        this.rx = rx;
        this.ry = ry;
        this.len = len;
        this.angle = angle;
        this.canvas = canvas;
        this.context = context;
    }
    draw() {
        this.base = new Line(this.rx - 50, this.ry, 0, 100, this.canvas, this.context);
        this.base.draw1();
        this.string = new Line(this.rx, this.ry, Math.PI / 2, this.len, this.canvas, this.context);
        this.string.draw1();
        this.bob = new Circle(this.rx, this.ry + this.len, this.len / 5, this.canvas, this.context);
        this.bob.draw1();
    }
}
class Sine {
    constructor(rx, ry, angle, len, canvas, context) {
        this.rx = rx;
        this.ry = ry;
        this.len = len;
        this.angle = angle;
        this.canvas = canvas;
        this.context = context;
    }
    draw() {
        this.base = new Line(this.rx - 50, this.ry + 100, 0, 4.5 * this.len, this.canvas, this.context);
        this.base.draw1();
        this.string = new Line(this.rx, this.ry, Math.PI / 2, 4 * this.len, this.canvas, this.context);
        this.string.draw1();
        this.bob = new Curve(this.rx, this.ry + this.len, this.len / 5, this.canvas, this.context);
        this.bob.draw1();
        this.base1 = new Line(this.rx - 50, this.ry + 300, 0, 4.5 * this.len, this.canvas, this.context);
        this.base1.draw1();
    }
}
//# sourceMappingURL=app.js.map