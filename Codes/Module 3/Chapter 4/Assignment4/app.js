var t1 = document.getElementById("t1");
var num = +t1.value;
var rr;
class Needle {
    constructor(rx, ry, len, canvas, context) {
        this.rx = rx;
        this.ry = ry;
        this.len = len;
        this.canvas = canvas;
        this.context = context;
    }
    draw1() {
        this.context.beginPath();
        this.context.moveTo(this.rx + 88, this.ry + 28);
        this.context.lineTo(this.rx + 2 * rr * 2 + num, this.ry + 60 + num);
        this.context.strokeStyle = "black";
        this.context.lineWidth = 2;
        this.context.stroke();
    }
}
class Circle {
    constructor(rx, ry, r, canvas, context) {
        this.rx = rx;
        this.ry = ry;
        this.r = r;
        this.canvas = canvas;
        this.context = context;
    }
    draw1() {
        this.context.beginPath();
        for (this.ang = 0; this.ang < 360; this.ang += 30) {
            this.ra = this.ang * Math.PI / 180;
            this.context.rotate(-this.ra);
            this.context.translate(0, -this.r - 14);
            this.context.rotate(+this.ra);
            this.context.fillStyle = "blue";
            this.context.fillText((this.ang).toString(), this.rx + 2 * this.r * 2, this.ry + 60);
        }
        this.context.arc(this.rx + 88, this.ry + 28, this.r * 2, 0, 2 * Math.PI);
        this.context.strokeStyle = "black";
        this.context.lineWidth = 2;
        this.context.stroke();
    }
}
class Dial {
    constructor(rx, ry, r, len, canvas, context) {
        this.rr = this.r;
        this.rx = rx;
        this.ry = ry;
        this.r = r;
        this.len;
        this.canvas = canvas;
        this.context = context;
    }
    draw() {
        this.curve = new Circle(this.rx, this.ry, this.r, this.canvas, this.context);
        this.curve.draw1();
        this.line = new Needle(this.rx, this.ry, this.len, this.canvas, this.context);
        this.line.draw1();
    }
}
//# sourceMappingURL=app.js.map