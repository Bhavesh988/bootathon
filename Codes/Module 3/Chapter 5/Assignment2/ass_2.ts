var canvas:HTMLCanvasElement=<HTMLCanvasElement>document.getElementById("myc");
var context:CanvasRenderingContext2D=canvas.getContext('2d');

var pen:pendulum=new pendulum(950,150,Math.PI/2,200,canvas,context);
pen.draw();

var sine:Sine=new Sine(75,100,Math.PI/4,100,canvas,context);
sine.draw();