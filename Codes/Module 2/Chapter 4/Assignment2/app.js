function Draw() {
    var t1 = document.getElementById("input");
    var table = document.getElementById("table");
    var num = +t1.value;
    var c = 1;
    var c1 = 1;
    var c2 = 1;
    var c3 = 1;
    if (isNaN(num)) //Exception handling to check if input is a number or not
     {
        alert("Enter a valid number");
        c1 = 1;
    }
    else {
        c1 = 0;
    }
    if (parseInt(t1.value) != num) //Exception handling to check if the number is an integer or not
     {
        alert("Enter integer numbers only");
        c3 = 1;
    }
    else {
        c3 = 0;
    }
    if (t1.value.length == 0) //Exception handling to check if a number has been inputted or not
     {
        alert("Input a number !");
        c = 1;
    }
    else {
        c = 0;
    }
    if (+t1.value <= 0) //Exception handling to check if inputted number is +ve or not
     {
        alert("Enter only positive numbers i.e numbers greater than 0");
        c2 = 1;
    }
    else {
        c2 = 0;
    }
    if (c1 == 0 && c == 0 && c2 == 0 && c3 == 0) {
        while (table.rows.length > 1) {
            table.deleteRow(1); //deleting rows
        }
        var c = 1;
        var d = num;
        while (c <= num) {
            var row = table.insertRow(); //creating new row
            var cell = row.insertCell(); //creating new cell
            var tt = document.createElement("input");
            tt.type = "text";
            tt.style.textAlign = "center";
            tt.value = num.toString();
            cell.appendChild(tt); //inserting tt value in cell
            var cell = row.insertCell(); //creating new cell
            var tt1 = document.createElement("input");
            var a = "*";
            tt1.type = "text";
            tt1.style.textAlign = "center";
            tt1.value = a;
            cell.appendChild(tt1);
            var cell = row.insertCell(); //creating new cell
            var tt2 = document.createElement("input");
            tt2.type = "text";
            tt2.style.textAlign = "center";
            tt2.value = c.toString();
            cell.appendChild(tt2);
            var cell = row.insertCell(); //creating new cell
            var tt3 = document.createElement("input");
            var a1 = "=";
            tt3.type = "text";
            tt3.style.textAlign = "center";
            tt3.value = a1;
            cell.appendChild(tt3);
            var cell = row.insertCell();
            var tt4 = document.createElement("input"); //creating new cell
            tt4.type = "text";
            tt4.style.textAlign = "center";
            tt4.value = (num * c).toString();
            cell.appendChild(tt4);
            c++;
        }
    }
}
//# sourceMappingURL=app.js.map